package eu.helpcent.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import eu.helpcent.web.rest.TestUtil;

public class HelpeeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Helpee.class);
        Helpee helpee1 = new Helpee();
        helpee1.setId(1L);
        Helpee helpee2 = new Helpee();
        helpee2.setId(helpee1.getId());
        assertThat(helpee1).isEqualTo(helpee2);
        helpee2.setId(2L);
        assertThat(helpee1).isNotEqualTo(helpee2);
        helpee1.setId(null);
        assertThat(helpee1).isNotEqualTo(helpee2);
    }
}
