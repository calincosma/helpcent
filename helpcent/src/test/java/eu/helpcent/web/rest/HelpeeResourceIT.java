package eu.helpcent.web.rest;

import eu.helpcent.HelpcentApp;
import eu.helpcent.domain.Helpee;
import eu.helpcent.repository.HelpeeRepository;
import eu.helpcent.service.HelpeeService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link HelpeeResource} REST controller.
 */
@SpringBootTest(classes = HelpcentApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class HelpeeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_STREET = "AAAAAAAAAA";
    private static final String UPDATED_STREET = "BBBBBBBBBB";

    private static final String DEFAULT_STREET_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_STREET_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_TOWNSHIP = "AAAAAAAAAA";
    private static final String UPDATED_TOWNSHIP = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_PAYPAL_ACCOUNT = "AAAAAAAAAA";
    private static final String UPDATED_PAYPAL_ACCOUNT = "BBBBBBBBBB";

    @Autowired
    private HelpeeRepository helpeeRepository;

    @Autowired
    private HelpeeService helpeeService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHelpeeMockMvc;

    private Helpee helpee;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Helpee createEntity(EntityManager em) {
        Helpee helpee = new Helpee()
            .name(DEFAULT_NAME)
            .street(DEFAULT_STREET)
            .streetNumber(DEFAULT_STREET_NUMBER)
            .township(DEFAULT_TOWNSHIP)
            .postalCode(DEFAULT_POSTAL_CODE)
            .country(DEFAULT_COUNTRY)
            .paypalAccount(DEFAULT_PAYPAL_ACCOUNT);
        return helpee;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Helpee createUpdatedEntity(EntityManager em) {
        Helpee helpee = new Helpee()
            .name(UPDATED_NAME)
            .street(UPDATED_STREET)
            .streetNumber(UPDATED_STREET_NUMBER)
            .township(UPDATED_TOWNSHIP)
            .postalCode(UPDATED_POSTAL_CODE)
            .country(UPDATED_COUNTRY)
            .paypalAccount(UPDATED_PAYPAL_ACCOUNT);
        return helpee;
    }

    @BeforeEach
    public void initTest() {
        helpee = createEntity(em);
    }

    @Test
    @Transactional
    public void createHelpee() throws Exception {
        int databaseSizeBeforeCreate = helpeeRepository.findAll().size();

        // Create the Helpee
        restHelpeeMockMvc.perform(post("/api/helpees").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(helpee)))
            .andExpect(status().isCreated());

        // Validate the Helpee in the database
        List<Helpee> helpeeList = helpeeRepository.findAll();
        assertThat(helpeeList).hasSize(databaseSizeBeforeCreate + 1);
        Helpee testHelpee = helpeeList.get(helpeeList.size() - 1);
        assertThat(testHelpee.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testHelpee.getStreet()).isEqualTo(DEFAULT_STREET);
        assertThat(testHelpee.getStreetNumber()).isEqualTo(DEFAULT_STREET_NUMBER);
        assertThat(testHelpee.getTownship()).isEqualTo(DEFAULT_TOWNSHIP);
        assertThat(testHelpee.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testHelpee.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testHelpee.getPaypalAccount()).isEqualTo(DEFAULT_PAYPAL_ACCOUNT);
    }

    @Test
    @Transactional
    public void createHelpeeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = helpeeRepository.findAll().size();

        // Create the Helpee with an existing ID
        helpee.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHelpeeMockMvc.perform(post("/api/helpees").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(helpee)))
            .andExpect(status().isBadRequest());

        // Validate the Helpee in the database
        List<Helpee> helpeeList = helpeeRepository.findAll();
        assertThat(helpeeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = helpeeRepository.findAll().size();
        // set the field null
        helpee.setName(null);

        // Create the Helpee, which fails.

        restHelpeeMockMvc.perform(post("/api/helpees").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(helpee)))
            .andExpect(status().isBadRequest());

        List<Helpee> helpeeList = helpeeRepository.findAll();
        assertThat(helpeeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllHelpees() throws Exception {
        // Initialize the database
        helpeeRepository.saveAndFlush(helpee);

        // Get all the helpeeList
        restHelpeeMockMvc.perform(get("/api/helpees?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(helpee.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].street").value(hasItem(DEFAULT_STREET)))
            .andExpect(jsonPath("$.[*].streetNumber").value(hasItem(DEFAULT_STREET_NUMBER)))
            .andExpect(jsonPath("$.[*].township").value(hasItem(DEFAULT_TOWNSHIP)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].paypalAccount").value(hasItem(DEFAULT_PAYPAL_ACCOUNT)));
    }
    
    @Test
    @Transactional
    public void getHelpee() throws Exception {
        // Initialize the database
        helpeeRepository.saveAndFlush(helpee);

        // Get the helpee
        restHelpeeMockMvc.perform(get("/api/helpees/{id}", helpee.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(helpee.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.street").value(DEFAULT_STREET))
            .andExpect(jsonPath("$.streetNumber").value(DEFAULT_STREET_NUMBER))
            .andExpect(jsonPath("$.township").value(DEFAULT_TOWNSHIP))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY))
            .andExpect(jsonPath("$.paypalAccount").value(DEFAULT_PAYPAL_ACCOUNT));
    }

    @Test
    @Transactional
    public void getNonExistingHelpee() throws Exception {
        // Get the helpee
        restHelpeeMockMvc.perform(get("/api/helpees/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHelpee() throws Exception {
        // Initialize the database
        helpeeService.save(helpee);

        int databaseSizeBeforeUpdate = helpeeRepository.findAll().size();

        // Update the helpee
        Helpee updatedHelpee = helpeeRepository.findById(helpee.getId()).get();
        // Disconnect from session so that the updates on updatedHelpee are not directly saved in db
        em.detach(updatedHelpee);
        updatedHelpee
            .name(UPDATED_NAME)
            .street(UPDATED_STREET)
            .streetNumber(UPDATED_STREET_NUMBER)
            .township(UPDATED_TOWNSHIP)
            .postalCode(UPDATED_POSTAL_CODE)
            .country(UPDATED_COUNTRY)
            .paypalAccount(UPDATED_PAYPAL_ACCOUNT);

        restHelpeeMockMvc.perform(put("/api/helpees").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedHelpee)))
            .andExpect(status().isOk());

        // Validate the Helpee in the database
        List<Helpee> helpeeList = helpeeRepository.findAll();
        assertThat(helpeeList).hasSize(databaseSizeBeforeUpdate);
        Helpee testHelpee = helpeeList.get(helpeeList.size() - 1);
        assertThat(testHelpee.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testHelpee.getStreet()).isEqualTo(UPDATED_STREET);
        assertThat(testHelpee.getStreetNumber()).isEqualTo(UPDATED_STREET_NUMBER);
        assertThat(testHelpee.getTownship()).isEqualTo(UPDATED_TOWNSHIP);
        assertThat(testHelpee.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testHelpee.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testHelpee.getPaypalAccount()).isEqualTo(UPDATED_PAYPAL_ACCOUNT);
    }

    @Test
    @Transactional
    public void updateNonExistingHelpee() throws Exception {
        int databaseSizeBeforeUpdate = helpeeRepository.findAll().size();

        // Create the Helpee

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHelpeeMockMvc.perform(put("/api/helpees").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(helpee)))
            .andExpect(status().isBadRequest());

        // Validate the Helpee in the database
        List<Helpee> helpeeList = helpeeRepository.findAll();
        assertThat(helpeeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteHelpee() throws Exception {
        // Initialize the database
        helpeeService.save(helpee);

        int databaseSizeBeforeDelete = helpeeRepository.findAll().size();

        // Delete the helpee
        restHelpeeMockMvc.perform(delete("/api/helpees/{id}", helpee.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Helpee> helpeeList = helpeeRepository.findAll();
        assertThat(helpeeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
