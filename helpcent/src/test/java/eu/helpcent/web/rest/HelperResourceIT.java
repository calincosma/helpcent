package eu.helpcent.web.rest;

import eu.helpcent.HelpcentApp;
import eu.helpcent.domain.Helper;
import eu.helpcent.repository.HelperRepository;
import eu.helpcent.service.HelperService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link HelperResource} REST controller.
 */
@SpringBootTest(classes = HelpcentApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class HelperResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PAYPAL_ACCOUNT = "AAAAAAAAAA";
    private static final String UPDATED_PAYPAL_ACCOUNT = "BBBBBBBBBB";

    @Autowired
    private HelperRepository helperRepository;

    @Autowired
    private HelperService helperService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHelperMockMvc;

    private Helper helper;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Helper createEntity(EntityManager em) {
        Helper helper = new Helper()
            .name(DEFAULT_NAME)
            .paypalAccount(DEFAULT_PAYPAL_ACCOUNT);
        return helper;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Helper createUpdatedEntity(EntityManager em) {
        Helper helper = new Helper()
            .name(UPDATED_NAME)
            .paypalAccount(UPDATED_PAYPAL_ACCOUNT);
        return helper;
    }

    @BeforeEach
    public void initTest() {
        helper = createEntity(em);
    }

    @Test
    @Transactional
    public void createHelper() throws Exception {
        int databaseSizeBeforeCreate = helperRepository.findAll().size();

        // Create the Helper
        restHelperMockMvc.perform(post("/api/helpers").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(helper)))
            .andExpect(status().isCreated());

        // Validate the Helper in the database
        List<Helper> helperList = helperRepository.findAll();
        assertThat(helperList).hasSize(databaseSizeBeforeCreate + 1);
        Helper testHelper = helperList.get(helperList.size() - 1);
        assertThat(testHelper.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testHelper.getPaypalAccount()).isEqualTo(DEFAULT_PAYPAL_ACCOUNT);
    }

    @Test
    @Transactional
    public void createHelperWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = helperRepository.findAll().size();

        // Create the Helper with an existing ID
        helper.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHelperMockMvc.perform(post("/api/helpers").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(helper)))
            .andExpect(status().isBadRequest());

        // Validate the Helper in the database
        List<Helper> helperList = helperRepository.findAll();
        assertThat(helperList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = helperRepository.findAll().size();
        // set the field null
        helper.setName(null);

        // Create the Helper, which fails.

        restHelperMockMvc.perform(post("/api/helpers").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(helper)))
            .andExpect(status().isBadRequest());

        List<Helper> helperList = helperRepository.findAll();
        assertThat(helperList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllHelpers() throws Exception {
        // Initialize the database
        helperRepository.saveAndFlush(helper);

        // Get all the helperList
        restHelperMockMvc.perform(get("/api/helpers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(helper.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].paypalAccount").value(hasItem(DEFAULT_PAYPAL_ACCOUNT)));
    }
    
    @Test
    @Transactional
    public void getHelper() throws Exception {
        // Initialize the database
        helperRepository.saveAndFlush(helper);

        // Get the helper
        restHelperMockMvc.perform(get("/api/helpers/{id}", helper.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(helper.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.paypalAccount").value(DEFAULT_PAYPAL_ACCOUNT));
    }

    @Test
    @Transactional
    public void getNonExistingHelper() throws Exception {
        // Get the helper
        restHelperMockMvc.perform(get("/api/helpers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHelper() throws Exception {
        // Initialize the database
        helperService.save(helper);

        int databaseSizeBeforeUpdate = helperRepository.findAll().size();

        // Update the helper
        Helper updatedHelper = helperRepository.findById(helper.getId()).get();
        // Disconnect from session so that the updates on updatedHelper are not directly saved in db
        em.detach(updatedHelper);
        updatedHelper
            .name(UPDATED_NAME)
            .paypalAccount(UPDATED_PAYPAL_ACCOUNT);

        restHelperMockMvc.perform(put("/api/helpers").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedHelper)))
            .andExpect(status().isOk());

        // Validate the Helper in the database
        List<Helper> helperList = helperRepository.findAll();
        assertThat(helperList).hasSize(databaseSizeBeforeUpdate);
        Helper testHelper = helperList.get(helperList.size() - 1);
        assertThat(testHelper.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testHelper.getPaypalAccount()).isEqualTo(UPDATED_PAYPAL_ACCOUNT);
    }

    @Test
    @Transactional
    public void updateNonExistingHelper() throws Exception {
        int databaseSizeBeforeUpdate = helperRepository.findAll().size();

        // Create the Helper

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHelperMockMvc.perform(put("/api/helpers").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(helper)))
            .andExpect(status().isBadRequest());

        // Validate the Helper in the database
        List<Helper> helperList = helperRepository.findAll();
        assertThat(helperList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteHelper() throws Exception {
        // Initialize the database
        helperService.save(helper);

        int databaseSizeBeforeDelete = helperRepository.findAll().size();

        // Delete the helper
        restHelperMockMvc.perform(delete("/api/helpers/{id}", helper.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Helper> helperList = helperRepository.findAll();
        assertThat(helperList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
