import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HelpcentTestModule } from '../../../test.module';
import { HelpeeDetailComponent } from 'app/entities/helpee/helpee-detail.component';
import { Helpee } from 'app/shared/model/helpee.model';

describe('Component Tests', () => {
  describe('Helpee Management Detail Component', () => {
    let comp: HelpeeDetailComponent;
    let fixture: ComponentFixture<HelpeeDetailComponent>;
    const route = ({ data: of({ helpee: new Helpee(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HelpcentTestModule],
        declarations: [HelpeeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(HelpeeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HelpeeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load helpee on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.helpee).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
