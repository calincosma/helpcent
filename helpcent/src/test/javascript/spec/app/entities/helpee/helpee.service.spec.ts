import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HelpeeService } from 'app/entities/helpee/helpee.service';
import { IHelpee, Helpee } from 'app/shared/model/helpee.model';

describe('Service Tests', () => {
  describe('Helpee Service', () => {
    let injector: TestBed;
    let service: HelpeeService;
    let httpMock: HttpTestingController;
    let elemDefault: IHelpee;
    let expectedResult: IHelpee | IHelpee[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(HelpeeService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Helpee(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Helpee', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Helpee()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Helpee', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            street: 'BBBBBB',
            streetNumber: 'BBBBBB',
            township: 'BBBBBB',
            postalCode: 'BBBBBB',
            country: 'BBBBBB',
            paypalAccount: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Helpee', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            street: 'BBBBBB',
            streetNumber: 'BBBBBB',
            township: 'BBBBBB',
            postalCode: 'BBBBBB',
            country: 'BBBBBB',
            paypalAccount: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Helpee', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
