import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HelpcentTestModule } from '../../../test.module';
import { HelpeeUpdateComponent } from 'app/entities/helpee/helpee-update.component';
import { HelpeeService } from 'app/entities/helpee/helpee.service';
import { Helpee } from 'app/shared/model/helpee.model';

describe('Component Tests', () => {
  describe('Helpee Management Update Component', () => {
    let comp: HelpeeUpdateComponent;
    let fixture: ComponentFixture<HelpeeUpdateComponent>;
    let service: HelpeeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HelpcentTestModule],
        declarations: [HelpeeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(HelpeeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HelpeeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HelpeeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Helpee(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Helpee();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
