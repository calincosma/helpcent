import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HelpcentTestModule } from '../../../test.module';
import { HelperDetailComponent } from 'app/entities/helper/helper-detail.component';
import { Helper } from 'app/shared/model/helper.model';

describe('Component Tests', () => {
  describe('Helper Management Detail Component', () => {
    let comp: HelperDetailComponent;
    let fixture: ComponentFixture<HelperDetailComponent>;
    const route = ({ data: of({ helper: new Helper(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HelpcentTestModule],
        declarations: [HelperDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(HelperDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HelperDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load helper on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.helper).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
