import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HelpcentTestModule } from '../../../test.module';
import { HelperUpdateComponent } from 'app/entities/helper/helper-update.component';
import { HelperService } from 'app/entities/helper/helper.service';
import { Helper } from 'app/shared/model/helper.model';

describe('Component Tests', () => {
  describe('Helper Management Update Component', () => {
    let comp: HelperUpdateComponent;
    let fixture: ComponentFixture<HelperUpdateComponent>;
    let service: HelperService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HelpcentTestModule],
        declarations: [HelperUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(HelperUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HelperUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HelperService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Helper(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Helper();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
