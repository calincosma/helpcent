import { Moment } from 'moment';

export interface ITransaction {
  id?: number;
  date?: Moment;
  amount?: number;
  donatedAmount?: number;
  success?: boolean;
  comments?: string;
}

export class Transaction implements ITransaction {
  constructor(
    public id?: number,
    public date?: Moment,
    public amount?: number,
    public donatedAmount?: number,
    public success?: boolean,
    public comments?: string
  ) {
    this.success = this.success || false;
  }
}
