import { Moment } from 'moment';
import { IHelper } from 'app/shared/model/helper.model';
import { IHelpee } from 'app/shared/model/helpee.model';

export interface IVoucher {
  id?: number;
  date?: Moment;
  expirationDate?: Moment;
  amount?: number;
  code?: string;
  used?: boolean;
  useDate?: Moment;
  helper?: IHelper;
  helpee?: IHelpee;
}

export class Voucher implements IVoucher {
  constructor(
    public id?: number,
    public date?: Moment,
    public expirationDate?: Moment,
    public amount?: number,
    public code?: string,
    public used?: boolean,
    public useDate?: Moment,
    public helper?: IHelper,
    public helpee?: IHelpee
  ) {
    this.used = this.used || false;
  }
}
