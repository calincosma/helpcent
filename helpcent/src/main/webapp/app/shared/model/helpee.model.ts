export interface IHelpee {
  id?: number;
  name?: string;
  street?: string;
  streetNumber?: string;
  township?: string;
  postalCode?: string;
  country?: string;
  paypalAccount?: string;
}

export class Helpee implements IHelpee {
  constructor(
    public id?: number,
    public name?: string,
    public street?: string,
    public streetNumber?: string,
    public township?: string,
    public postalCode?: string,
    public country?: string,
    public paypalAccount?: string
  ) {}
}
