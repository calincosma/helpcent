export interface IMerchant {
  id?: number;
  name?: string;
  paypalAccount?: string;
}

export class Merchant implements IMerchant {
  constructor(public id?: number, public name?: string, public paypalAccount?: string) {}
}
