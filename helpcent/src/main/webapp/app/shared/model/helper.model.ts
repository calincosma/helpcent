export interface IHelper {
  id?: number;
  name?: string;
  paypalAccount?: string;
}

export class Helper implements IHelper {
  constructor(public id?: number, public name?: string, public paypalAccount?: string) {}
}
