import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IHelpee } from 'app/shared/model/helpee.model';

@Component({
  selector: 'jhi-helpee-detail',
  templateUrl: './helpee-detail.component.html'
})
export class HelpeeDetailComponent implements OnInit {
  helpee: IHelpee | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ helpee }) => (this.helpee = helpee));
  }

  previousState(): void {
    window.history.back();
  }
}
