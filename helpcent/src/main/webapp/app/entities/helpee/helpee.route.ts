import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IHelpee, Helpee } from 'app/shared/model/helpee.model';
import { HelpeeService } from './helpee.service';
import { HelpeeComponent } from './helpee.component';
import { HelpeeDetailComponent } from './helpee-detail.component';
import { HelpeeUpdateComponent } from './helpee-update.component';

@Injectable({ providedIn: 'root' })
export class HelpeeResolve implements Resolve<IHelpee> {
  constructor(private service: HelpeeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IHelpee> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((helpee: HttpResponse<Helpee>) => {
          if (helpee.body) {
            return of(helpee.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Helpee());
  }
}

export const helpeeRoute: Routes = [
  {
    path: '',
    component: HelpeeComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'helpcentApp.helpee.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: HelpeeDetailComponent,
    resolve: {
      helpee: HelpeeResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'helpcentApp.helpee.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: HelpeeUpdateComponent,
    resolve: {
      helpee: HelpeeResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'helpcentApp.helpee.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: HelpeeUpdateComponent,
    resolve: {
      helpee: HelpeeResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'helpcentApp.helpee.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
