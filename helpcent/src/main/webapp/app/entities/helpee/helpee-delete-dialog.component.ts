import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHelpee } from 'app/shared/model/helpee.model';
import { HelpeeService } from './helpee.service';

@Component({
  templateUrl: './helpee-delete-dialog.component.html'
})
export class HelpeeDeleteDialogComponent {
  helpee?: IHelpee;

  constructor(protected helpeeService: HelpeeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.helpeeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('helpeeListModification');
      this.activeModal.close();
    });
  }
}
