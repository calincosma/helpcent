import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelpcentSharedModule } from 'app/shared/shared.module';
import { HelpeeComponent } from './helpee.component';
import { HelpeeDetailComponent } from './helpee-detail.component';
import { HelpeeUpdateComponent } from './helpee-update.component';
import { HelpeeDeleteDialogComponent } from './helpee-delete-dialog.component';
import { helpeeRoute } from './helpee.route';

@NgModule({
  imports: [HelpcentSharedModule, RouterModule.forChild(helpeeRoute)],
  declarations: [HelpeeComponent, HelpeeDetailComponent, HelpeeUpdateComponent, HelpeeDeleteDialogComponent],
  entryComponents: [HelpeeDeleteDialogComponent]
})
export class HelpcentHelpeeModule {}
