import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IHelpee, Helpee } from 'app/shared/model/helpee.model';
import { HelpeeService } from './helpee.service';

@Component({
  selector: 'jhi-helpee-update',
  templateUrl: './helpee-update.component.html'
})
export class HelpeeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    street: [],
    streetNumber: [],
    township: [],
    postalCode: [],
    country: [],
    paypalAccount: []
  });

  constructor(protected helpeeService: HelpeeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ helpee }) => {
      this.updateForm(helpee);
    });
  }

  updateForm(helpee: IHelpee): void {
    this.editForm.patchValue({
      id: helpee.id,
      name: helpee.name,
      street: helpee.street,
      streetNumber: helpee.streetNumber,
      township: helpee.township,
      postalCode: helpee.postalCode,
      country: helpee.country,
      paypalAccount: helpee.paypalAccount
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const helpee = this.createFromForm();
    if (helpee.id !== undefined) {
      this.subscribeToSaveResponse(this.helpeeService.update(helpee));
    } else {
      this.subscribeToSaveResponse(this.helpeeService.create(helpee));
    }
  }

  private createFromForm(): IHelpee {
    return {
      ...new Helpee(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      street: this.editForm.get(['street'])!.value,
      streetNumber: this.editForm.get(['streetNumber'])!.value,
      township: this.editForm.get(['township'])!.value,
      postalCode: this.editForm.get(['postalCode'])!.value,
      country: this.editForm.get(['country'])!.value,
      paypalAccount: this.editForm.get(['paypalAccount'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHelpee>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
