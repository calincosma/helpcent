import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IHelpee } from 'app/shared/model/helpee.model';

type EntityResponseType = HttpResponse<IHelpee>;
type EntityArrayResponseType = HttpResponse<IHelpee[]>;

@Injectable({ providedIn: 'root' })
export class HelpeeService {
  public resourceUrl = SERVER_API_URL + 'api/helpees';

  constructor(protected http: HttpClient) {}

  create(helpee: IHelpee): Observable<EntityResponseType> {
    return this.http.post<IHelpee>(this.resourceUrl, helpee, { observe: 'response' });
  }

  update(helpee: IHelpee): Observable<EntityResponseType> {
    return this.http.put<IHelpee>(this.resourceUrl, helpee, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IHelpee>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IHelpee[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
