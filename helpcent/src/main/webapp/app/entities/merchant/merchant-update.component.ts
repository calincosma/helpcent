import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IMerchant, Merchant } from 'app/shared/model/merchant.model';
import { MerchantService } from './merchant.service';

@Component({
  selector: 'jhi-merchant-update',
  templateUrl: './merchant-update.component.html'
})
export class MerchantUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    paypalAccount: []
  });

  constructor(protected merchantService: MerchantService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ merchant }) => {
      this.updateForm(merchant);
    });
  }

  updateForm(merchant: IMerchant): void {
    this.editForm.patchValue({
      id: merchant.id,
      name: merchant.name,
      paypalAccount: merchant.paypalAccount
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const merchant = this.createFromForm();
    if (merchant.id !== undefined) {
      this.subscribeToSaveResponse(this.merchantService.update(merchant));
    } else {
      this.subscribeToSaveResponse(this.merchantService.create(merchant));
    }
  }

  private createFromForm(): IMerchant {
    return {
      ...new Merchant(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      paypalAccount: this.editForm.get(['paypalAccount'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMerchant>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
