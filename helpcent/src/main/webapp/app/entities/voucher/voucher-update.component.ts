import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IVoucher, Voucher } from 'app/shared/model/voucher.model';
import { VoucherService } from './voucher.service';
import { IHelper } from 'app/shared/model/helper.model';
import { HelperService } from 'app/entities/helper/helper.service';
import { IHelpee } from 'app/shared/model/helpee.model';
import { HelpeeService } from 'app/entities/helpee/helpee.service';

type SelectableEntity = IHelper | IHelpee;

@Component({
  selector: 'jhi-voucher-update',
  templateUrl: './voucher-update.component.html'
})
export class VoucherUpdateComponent implements OnInit {
  isSaving = false;
  helpers: IHelper[] = [];
  helpees: IHelpee[] = [];
  dateDp: any;
  expirationDateDp: any;
  useDateDp: any;

  editForm = this.fb.group({
    id: [],
    date: [],
    expirationDate: [],
    amount: [],
    code: [],
    used: [],
    useDate: [],
    helper: [],
    helpee: []
  });

  constructor(
    protected voucherService: VoucherService,
    protected helperService: HelperService,
    protected helpeeService: HelpeeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ voucher }) => {
      this.updateForm(voucher);

      this.helperService.query().subscribe((res: HttpResponse<IHelper[]>) => (this.helpers = res.body || []));

      this.helpeeService.query().subscribe((res: HttpResponse<IHelpee[]>) => (this.helpees = res.body || []));
    });
  }

  updateForm(voucher: IVoucher): void {
    this.editForm.patchValue({
      id: voucher.id,
      date: voucher.date,
      expirationDate: voucher.expirationDate,
      amount: voucher.amount,
      code: voucher.code,
      used: voucher.used,
      useDate: voucher.useDate,
      helper: voucher.helper,
      helpee: voucher.helpee
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const voucher = this.createFromForm();
    if (voucher.id !== undefined) {
      this.subscribeToSaveResponse(this.voucherService.update(voucher));
    } else {
      this.subscribeToSaveResponse(this.voucherService.create(voucher));
    }
  }

  private createFromForm(): IVoucher {
    return {
      ...new Voucher(),
      id: this.editForm.get(['id'])!.value,
      date: this.editForm.get(['date'])!.value,
      expirationDate: this.editForm.get(['expirationDate'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      code: this.editForm.get(['code'])!.value,
      used: this.editForm.get(['used'])!.value,
      useDate: this.editForm.get(['useDate'])!.value,
      helper: this.editForm.get(['helper'])!.value,
      helpee: this.editForm.get(['helpee'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVoucher>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
