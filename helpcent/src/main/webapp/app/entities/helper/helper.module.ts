import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelpcentSharedModule } from 'app/shared/shared.module';
import { HelperComponent } from './helper.component';
import { HelperDetailComponent } from './helper-detail.component';
import { HelperUpdateComponent } from './helper-update.component';
import { HelperDeleteDialogComponent } from './helper-delete-dialog.component';
import { helperRoute } from './helper.route';

@NgModule({
  imports: [HelpcentSharedModule, RouterModule.forChild(helperRoute)],
  declarations: [HelperComponent, HelperDetailComponent, HelperUpdateComponent, HelperDeleteDialogComponent],
  entryComponents: [HelperDeleteDialogComponent]
})
export class HelpcentHelperModule {}
