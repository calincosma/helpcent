import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IHelper, Helper } from 'app/shared/model/helper.model';
import { HelperService } from './helper.service';

@Component({
  selector: 'jhi-helper-update',
  templateUrl: './helper-update.component.html'
})
export class HelperUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    paypalAccount: []
  });

  constructor(protected helperService: HelperService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ helper }) => {
      this.updateForm(helper);
    });
  }

  updateForm(helper: IHelper): void {
    this.editForm.patchValue({
      id: helper.id,
      name: helper.name,
      paypalAccount: helper.paypalAccount
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const helper = this.createFromForm();
    if (helper.id !== undefined) {
      this.subscribeToSaveResponse(this.helperService.update(helper));
    } else {
      this.subscribeToSaveResponse(this.helperService.create(helper));
    }
  }

  private createFromForm(): IHelper {
    return {
      ...new Helper(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      paypalAccount: this.editForm.get(['paypalAccount'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHelper>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
