import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IHelper } from 'app/shared/model/helper.model';

type EntityResponseType = HttpResponse<IHelper>;
type EntityArrayResponseType = HttpResponse<IHelper[]>;

@Injectable({ providedIn: 'root' })
export class HelperService {
  public resourceUrl = SERVER_API_URL + 'api/helpers';

  constructor(protected http: HttpClient) {}

  create(helper: IHelper): Observable<EntityResponseType> {
    return this.http.post<IHelper>(this.resourceUrl, helper, { observe: 'response' });
  }

  update(helper: IHelper): Observable<EntityResponseType> {
    return this.http.put<IHelper>(this.resourceUrl, helper, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IHelper>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IHelper[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
