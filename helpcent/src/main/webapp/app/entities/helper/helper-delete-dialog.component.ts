import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHelper } from 'app/shared/model/helper.model';
import { HelperService } from './helper.service';

@Component({
  templateUrl: './helper-delete-dialog.component.html'
})
export class HelperDeleteDialogComponent {
  helper?: IHelper;

  constructor(protected helperService: HelperService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.helperService.delete(id).subscribe(() => {
      this.eventManager.broadcast('helperListModification');
      this.activeModal.close();
    });
  }
}
