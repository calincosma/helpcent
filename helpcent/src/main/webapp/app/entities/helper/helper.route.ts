import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IHelper, Helper } from 'app/shared/model/helper.model';
import { HelperService } from './helper.service';
import { HelperComponent } from './helper.component';
import { HelperDetailComponent } from './helper-detail.component';
import { HelperUpdateComponent } from './helper-update.component';

@Injectable({ providedIn: 'root' })
export class HelperResolve implements Resolve<IHelper> {
  constructor(private service: HelperService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IHelper> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((helper: HttpResponse<Helper>) => {
          if (helper.body) {
            return of(helper.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Helper());
  }
}

export const helperRoute: Routes = [
  {
    path: '',
    component: HelperComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'helpcentApp.helper.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: HelperDetailComponent,
    resolve: {
      helper: HelperResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'helpcentApp.helper.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: HelperUpdateComponent,
    resolve: {
      helper: HelperResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'helpcentApp.helper.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: HelperUpdateComponent,
    resolve: {
      helper: HelperResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'helpcentApp.helper.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
