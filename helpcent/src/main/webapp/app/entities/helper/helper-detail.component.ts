import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IHelper } from 'app/shared/model/helper.model';

@Component({
  selector: 'jhi-helper-detail',
  templateUrl: './helper-detail.component.html'
})
export class HelperDetailComponent implements OnInit {
  helper: IHelper | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ helper }) => (this.helper = helper));
  }

  previousState(): void {
    window.history.back();
  }
}
