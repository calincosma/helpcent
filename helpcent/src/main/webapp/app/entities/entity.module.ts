import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'helpee',
        loadChildren: () => import('./helpee/helpee.module').then(m => m.HelpcentHelpeeModule)
      },
      {
        path: 'helper',
        loadChildren: () => import('./helper/helper.module').then(m => m.HelpcentHelperModule)
      },
      {
        path: 'merchant',
        loadChildren: () => import('./merchant/merchant.module').then(m => m.HelpcentMerchantModule)
      },
      {
        path: 'voucher',
        loadChildren: () => import('./voucher/voucher.module').then(m => m.HelpcentVoucherModule)
      },
      {
        path: 'transaction',
        loadChildren: () => import('./transaction/transaction.module').then(m => m.HelpcentTransactionModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class HelpcentEntityModule {}
