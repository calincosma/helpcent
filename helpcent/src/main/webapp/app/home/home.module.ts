import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelpcentSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [HelpcentSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent]
})
export class HelpcentHomeModule {}
