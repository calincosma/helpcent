package eu.helpcent.web.rest;

import eu.helpcent.domain.Helper;
import eu.helpcent.service.HelperService;
import eu.helpcent.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link eu.helpcent.domain.Helper}.
 */
@RestController
@RequestMapping("/api")
public class HelperResource {

    private final Logger log = LoggerFactory.getLogger(HelperResource.class);

    private static final String ENTITY_NAME = "helper";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HelperService helperService;

    public HelperResource(HelperService helperService) {
        this.helperService = helperService;
    }

    /**
     * {@code POST  /helpers} : Create a new helper.
     *
     * @param helper the helper to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new helper, or with status {@code 400 (Bad Request)} if the helper has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/helpers")
    public ResponseEntity<Helper> createHelper(@Valid @RequestBody Helper helper) throws URISyntaxException {
        log.debug("REST request to save Helper : {}", helper);
        if (helper.getId() != null) {
            throw new BadRequestAlertException("A new helper cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Helper result = helperService.save(helper);
        return ResponseEntity.created(new URI("/api/helpers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /helpers} : Updates an existing helper.
     *
     * @param helper the helper to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated helper,
     * or with status {@code 400 (Bad Request)} if the helper is not valid,
     * or with status {@code 500 (Internal Server Error)} if the helper couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/helpers")
    public ResponseEntity<Helper> updateHelper(@Valid @RequestBody Helper helper) throws URISyntaxException {
        log.debug("REST request to update Helper : {}", helper);
        if (helper.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Helper result = helperService.save(helper);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, helper.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /helpers} : get all the helpers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of helpers in body.
     */
    @GetMapping("/helpers")
    public ResponseEntity<List<Helper>> getAllHelpers(Pageable pageable) {
        log.debug("REST request to get a page of Helpers");
        Page<Helper> page = helperService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /helpers/:id} : get the "id" helper.
     *
     * @param id the id of the helper to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the helper, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/helpers/{id}")
    public ResponseEntity<Helper> getHelper(@PathVariable Long id) {
        log.debug("REST request to get Helper : {}", id);
        Optional<Helper> helper = helperService.findOne(id);
        return ResponseUtil.wrapOrNotFound(helper);
    }

    /**
     * {@code DELETE  /helpers/:id} : delete the "id" helper.
     *
     * @param id the id of the helper to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/helpers/{id}")
    public ResponseEntity<Void> deleteHelper(@PathVariable Long id) {
        log.debug("REST request to delete Helper : {}", id);
        helperService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
