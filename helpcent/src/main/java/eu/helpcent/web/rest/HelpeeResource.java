package eu.helpcent.web.rest;

import eu.helpcent.domain.Helpee;
import eu.helpcent.service.HelpeeService;
import eu.helpcent.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link eu.helpcent.domain.Helpee}.
 */
@RestController
@RequestMapping("/api")
public class HelpeeResource {

    private final Logger log = LoggerFactory.getLogger(HelpeeResource.class);

    private static final String ENTITY_NAME = "helpee";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HelpeeService helpeeService;

    public HelpeeResource(HelpeeService helpeeService) {
        this.helpeeService = helpeeService;
    }

    /**
     * {@code POST  /helpees} : Create a new helpee.
     *
     * @param helpee the helpee to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new helpee, or with status {@code 400 (Bad Request)} if the helpee has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/helpees")
    public ResponseEntity<Helpee> createHelpee(@Valid @RequestBody Helpee helpee) throws URISyntaxException {
        log.debug("REST request to save Helpee : {}", helpee);
        if (helpee.getId() != null) {
            throw new BadRequestAlertException("A new helpee cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Helpee result = helpeeService.save(helpee);
        return ResponseEntity.created(new URI("/api/helpees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /helpees} : Updates an existing helpee.
     *
     * @param helpee the helpee to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated helpee,
     * or with status {@code 400 (Bad Request)} if the helpee is not valid,
     * or with status {@code 500 (Internal Server Error)} if the helpee couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/helpees")
    public ResponseEntity<Helpee> updateHelpee(@Valid @RequestBody Helpee helpee) throws URISyntaxException {
        log.debug("REST request to update Helpee : {}", helpee);
        if (helpee.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Helpee result = helpeeService.save(helpee);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, helpee.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /helpees} : get all the helpees.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of helpees in body.
     */
    @GetMapping("/helpees")
    public ResponseEntity<List<Helpee>> getAllHelpees(Pageable pageable) {
        log.debug("REST request to get a page of Helpees");
        Page<Helpee> page = helpeeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /helpees/:id} : get the "id" helpee.
     *
     * @param id the id of the helpee to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the helpee, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/helpees/{id}")
    public ResponseEntity<Helpee> getHelpee(@PathVariable Long id) {
        log.debug("REST request to get Helpee : {}", id);
        Optional<Helpee> helpee = helpeeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(helpee);
    }

    /**
     * {@code DELETE  /helpees/:id} : delete the "id" helpee.
     *
     * @param id the id of the helpee to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/helpees/{id}")
    public ResponseEntity<Void> deleteHelpee(@PathVariable Long id) {
        log.debug("REST request to delete Helpee : {}", id);
        helpeeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
