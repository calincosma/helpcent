/**
 * View Models used by Spring MVC REST controllers.
 */
package eu.helpcent.web.rest.vm;
