package eu.helpcent.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.LocalDate;
import java.util.UUID;

/**
 * A Voucher.
 */
@Entity
@Table(name = "voucher")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Voucher implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "expiration_date")
    private LocalDate expirationDate;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "code")
    private UUID code;

    @Column(name = "used")
    private Boolean used;

    @Column(name = "use_date")
    private LocalDate useDate;

    @ManyToOne
    @JsonIgnoreProperties("vouchers")
    private Helper helper;

    @ManyToOne
    @JsonIgnoreProperties("vouchers")
    private Helpee helpee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public Voucher date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public Voucher expirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Double getAmount() {
        return amount;
    }

    public Voucher amount(Double amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public UUID getCode() {
        return code;
    }

    public Voucher code(UUID code) {
        this.code = code;
        return this;
    }

    public void setCode(UUID code) {
        this.code = code;
    }

    public Boolean isUsed() {
        return used;
    }

    public Voucher used(Boolean used) {
        this.used = used;
        return this;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }

    public LocalDate getUseDate() {
        return useDate;
    }

    public Voucher useDate(LocalDate useDate) {
        this.useDate = useDate;
        return this;
    }

    public void setUseDate(LocalDate useDate) {
        this.useDate = useDate;
    }

    public Helper getHelper() {
        return helper;
    }

    public Voucher helper(Helper helper) {
        this.helper = helper;
        return this;
    }

    public void setHelper(Helper helper) {
        this.helper = helper;
    }

    public Helpee getHelpee() {
        return helpee;
    }

    public Voucher helpee(Helpee helpee) {
        this.helpee = helpee;
        return this;
    }

    public void setHelpee(Helpee helpee) {
        this.helpee = helpee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Voucher)) {
            return false;
        }
        return id != null && id.equals(((Voucher) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Voucher{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", expirationDate='" + getExpirationDate() + "'" +
            ", amount=" + getAmount() +
            ", code='" + getCode() + "'" +
            ", used='" + isUsed() + "'" +
            ", useDate='" + getUseDate() + "'" +
            "}";
    }
}
