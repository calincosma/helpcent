package eu.helpcent.repository;

import eu.helpcent.domain.Helpee;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Helpee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HelpeeRepository extends JpaRepository<Helpee, Long> {
}
