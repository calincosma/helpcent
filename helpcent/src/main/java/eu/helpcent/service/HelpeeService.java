package eu.helpcent.service;

import eu.helpcent.domain.Helpee;
import eu.helpcent.repository.HelpeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Helpee}.
 */
@Service
@Transactional
public class HelpeeService {

    private final Logger log = LoggerFactory.getLogger(HelpeeService.class);

    private final HelpeeRepository helpeeRepository;

    public HelpeeService(HelpeeRepository helpeeRepository) {
        this.helpeeRepository = helpeeRepository;
    }

    /**
     * Save a helpee.
     *
     * @param helpee the entity to save.
     * @return the persisted entity.
     */
    public Helpee save(Helpee helpee) {
        log.debug("Request to save Helpee : {}", helpee);
        return helpeeRepository.save(helpee);
    }

    /**
     * Get all the helpees.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Helpee> findAll(Pageable pageable) {
        log.debug("Request to get all Helpees");
        return helpeeRepository.findAll(pageable);
    }

    /**
     * Get one helpee by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Helpee> findOne(Long id) {
        log.debug("Request to get Helpee : {}", id);
        return helpeeRepository.findById(id);
    }

    /**
     * Delete the helpee by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Helpee : {}", id);
        helpeeRepository.deleteById(id);
    }
}
