package eu.helpcent.service;

import eu.helpcent.domain.Helper;
import eu.helpcent.repository.HelperRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Helper}.
 */
@Service
@Transactional
public class HelperService {

    private final Logger log = LoggerFactory.getLogger(HelperService.class);

    private final HelperRepository helperRepository;

    public HelperService(HelperRepository helperRepository) {
        this.helperRepository = helperRepository;
    }

    /**
     * Save a helper.
     *
     * @param helper the entity to save.
     * @return the persisted entity.
     */
    public Helper save(Helper helper) {
        log.debug("Request to save Helper : {}", helper);
        return helperRepository.save(helper);
    }

    /**
     * Get all the helpers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Helper> findAll(Pageable pageable) {
        log.debug("Request to get all Helpers");
        return helperRepository.findAll(pageable);
    }

    /**
     * Get one helper by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Helper> findOne(Long id) {
        log.debug("Request to get Helper : {}", id);
        return helperRepository.findById(id);
    }

    /**
     * Delete the helper by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Helper : {}", id);
        helperRepository.deleteById(id);
    }
}
