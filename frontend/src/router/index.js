import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Search from "../views/Search.vue";
import Business from "../views/Business.vue";
import HelpChoice from "../views/HelpChoice.vue";
import Login from "../views/Login.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/i-want-to-help",
    name: "HelpChoice",
    component: HelpChoice,
  },
  {
    path: "/search",
    name: "Search",
    component: Search,
  },
  {
    path: "/business/:id",
    name: "Business",
    component: Business,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
